package org.extendj.gradle

import nebula.test.PluginProjectSpec
import org.gradle.api.tasks.compile.JavaCompile

class ExtendJBasePluginSpec extends PluginProjectSpec {
  @Override String getPluginName() {
    return 'org.extendj-base'
  }

  def 'plugin should add extendj configuration'() {
    when:
    project.apply plugin: pluginName
    project.evaluate()

    then:
    project.configurations.findByName('extendj')
  }

  def 'plugin should not configure JavaCompile tasks'() {
    when:
    project.apply plugin: pluginName
    project.apply plugin: 'java'
    project.evaluate()

    then:
    project.configurations.findByName('extendj')
    !project.tasks.withType(JavaCompile).any { it.toolChain instanceof ExtendJToolChain }
  }
}
