package org.extendj.gradle

import nebula.test.PluginProjectSpec
import org.gradle.api.tasks.compile.JavaCompile

class ExtendJPluginSpec extends PluginProjectSpec {
  @Override String getPluginName() {
    return 'org.extendj'
  }


  def 'should apply org.extendj-base plugin'() {
    when:
    project.apply plugin: pluginName
    project.evaluate()

    then:
    project.plugins.hasPlugin('org.extendj-base')
    project.configurations.findByName('extendj')
  }

  def 'should configure all JavaCompile tasks'() {
    when:
    project.apply plugin: pluginName
    project.apply plugin: 'java'
    project.evaluate()

    then:
    project.plugins.hasPlugin('org.extendj-base')
    project.configurations.findByName('extendj')
    project.tasks.withType(JavaCompile).all { it.toolChain instanceof ExtendJToolChain }
  }
}
