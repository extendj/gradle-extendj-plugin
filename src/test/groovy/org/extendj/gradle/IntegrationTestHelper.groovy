package org.extendj.gradle

class IntegrationTestHelper {
    static final GRADLE_VERSIONS = System.getProperty("test.gradle-versions", "2.13,2.14,3.0,3.1,3.2,3.3,3.4,3.5,4.0,4.1,4.2,4.3,4.4")
            .tokenize(',');
}
