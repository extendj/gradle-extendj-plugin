package org.extendj.gradle

import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class ExtendJBasePluginIntegrationSpec extends Specification {
  @Rule final TemporaryFolder testProjectDir = new TemporaryFolder()
  File buildFile

  def setup() {
    buildFile = testProjectDir.newFile('build.gradle')
    buildFile << """\
      buildscript {
        dependencies {
          classpath files(\$/${System.getProperty('plugin')}/\$)
        }
      }
      apply plugin: 'org.extendj-base'
      apply plugin: 'java'

      repositories {
        mavenCentral()
      }
      dependencies {
        extendj fileTree(\$/${System.getProperty('dependencies')}/\$)
      }
""".stripIndent()

    def p = testProjectDir.newFolder('src', 'main', 'java', 'test')
    def f = new File(p, 'Success.java')
    f.createNewFile()
    getClass().getResource("/test/Success.java").withInputStream { f << it }
    f = new File(p, 'Failure.java')
    f.createNewFile()
    getClass().getResource("/test/Failure.java").withInputStream { f << it }
  }

  def "compilation succeeds even when using feature not supported by ExtendJ, because ExtendJ is not used"() {
    when:
    def result = GradleRunner.create()
        .withProjectDir(testProjectDir.root)
        .withArguments('--info', 'compileJava')
        .build()

    then:
    !result.output.contains('Compiling with ExtendJ compiler')
    result.task(':compileJava').outcome == TaskOutcome.SUCCESS
  }

  def "compilation succeeds (ExtendJ applied to compileJava only)"() {
    given:
    buildFile << """
      import org.extendj.gradle.ExtendJToolChain

      compileJava {
        include 'test/Success.java'
        toolChain ExtendJToolChain.create(project)
      }
      task compileFailure(type: JavaCompile) {
        classpath = sourceSets.main.compileClasspath
        destinationDir = sourceSets.main.output.classesDir
        source sourceSets.main.java
        include 'test/Failure.java'
      }
    """.stripIndent()

    when:
    def result = GradleRunner.create()
        .withProjectDir(testProjectDir.root)
        .withArguments('--info', 'compileJava', 'compileFailure')
        .build()

    then:
    result.output.contains('Compiling with ExtendJ compiler')
    result.task(':compileJava').outcome == TaskOutcome.SUCCESS
    result.task(':compileFailure').outcome == TaskOutcome.SUCCESS
  }
}
