package test;

// Legal Java code that fails to compile with ExtendJ.
public abstract class Failure {
  List<Integer> build() {
    return buildIt();
  }

  abstract <T, U extends List<T>> U buildIt();
}

interface List<T> {}
