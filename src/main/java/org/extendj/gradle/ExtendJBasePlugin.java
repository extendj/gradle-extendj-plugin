package org.extendj.gradle;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class ExtendJBasePlugin implements Plugin<Project> {

  public static final String CONFIGURATION_NAME = "extendj";

  private static final String DEFAULT_DEPENDENCY = "org.extendj:extendj8:latest.release";

  @Override
  public void apply(final Project project) {
    project
        .getConfigurations()
        .create(
            CONFIGURATION_NAME,
            files -> {
              files.setVisible(false);
              files.defaultDependencies(
                  dependencies ->
                      dependencies.add(project.getDependencies().create(DEFAULT_DEPENDENCY)));
            });
  }
}
