package org.extendj.gradle;

import java.io.PrintStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.internal.tasks.compile.CompilationFailedException;
import org.gradle.api.internal.tasks.compile.JavaCompileSpec;
import org.gradle.api.internal.tasks.compile.JavaCompilerArgumentsBuilder;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.api.tasks.WorkResult;
import org.gradle.internal.UncheckedException;
import org.gradle.language.base.internal.compile.Compiler;
import org.gradle.util.GradleVersion;

public class ExtendJCompiler implements Compiler<JavaCompileSpec> {
  private static final Logger LOG = Logging.getLogger(ExtendJCompiler.class);

  // Gradle 4.2 introduced WorkResults, and made SimpleWorkResult nag users.
  @SuppressWarnings("deprecation")
  private static final WorkResult DID_WORK =
      GradleVersion.current().compareTo(GradleVersion.version("4.2")) >= 0
          ? org.gradle.api.tasks.WorkResults.didWork(true)
          : new org.gradle.api.internal.tasks.SimpleWorkResult(true);

  private final Project project;
  private final Configuration extendj;

  public ExtendJCompiler(Project project, Configuration extendj) {
    this.project = project;
    this.extendj = extendj;
  }

  @Override
  public WorkResult execute(JavaCompileSpec spec) {
    LOG.info("Compiling with ExtendJ compiler");

    List<String> args = new JavaCompilerArgumentsBuilder(spec).includeSourceFiles(true).build();
    Object extendjLogProperty = project.findProperty("extendj.log");
    String extendjLog = extendjLogProperty == null
        ? ""
        : extendjLogProperty.toString().trim();
    if (!extendjLog.isEmpty()) {
      File logfile = new File(extendjLog);
      try {
        LOG.info("Writing compile argument to file: " + logfile.getAbsolutePath());
        FileOutputStream f = new FileOutputStream(logfile);
        PrintStream out = new PrintStream(f);
        for (String arg : args) {
          out.println(arg);
        }
        out.close();
        f.close();
      } catch (IOException e) {
        LOG.warn("Failed to write compile argument log: " + logfile.getAbsolutePath(), e);
      }
    }

    Set<String> removeArgs = new HashSet<>(Arrays.asList("-proc:none", "-XDuseUnsharedTable=true"));
    args.removeAll(removeArgs);

    URL[] urls =
        extendj
            .getFiles()
            .stream()
            .map(
                file -> {
                  try {
                    return file.toURI().toURL();
                  } catch (MalformedURLException e) {
                    throw UncheckedException.throwAsUncheckedException(e);
                  }
                })
            .toArray(URL[]::new);

    ClassLoader tccl = Thread.currentThread().getContextClassLoader();
    boolean pass;
    try (URLClassLoader cl = new SelfFirstClassLoader(urls)) {
      Thread.currentThread().setContextClassLoader(cl);

      Class<?> compilerClass = cl.loadClass("org.extendj.JavaCompiler");
      Object compiler = compilerClass.getConstructor().newInstance();
      Object result =
          compiler
              .getClass()
              .getMethod("compile", String[].class)
              .invoke(compiler, (Object) new String[] { "-version" });
      pass = (Boolean) result;
      if (pass) {
        result = compiler
            .getClass()
            .getMethod("compile", String[].class)
            .invoke(compiler, (Object) args.toArray(new String[args.size()]));
        pass = (Boolean) result;
      }
    } catch (Exception e) {
      throw UncheckedException.throwAsUncheckedException(e);
    } finally {
      Thread.currentThread().setContextClassLoader(tccl);
    }
    if (!pass) {
      throw new CompilationFailedException(1);
    }

    return DID_WORK;
  }

  private static class SelfFirstClassLoader extends URLClassLoader {

    private static final ClassLoader BOOTSTRAP_ONLY_CLASSLOADER = new ClassLoader(null) {};

    public SelfFirstClassLoader(URL[] urls) {
      super(urls, null);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
      return loadClass(name, false);
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
      synchronized (getClassLoadingLock(name)) {
        Class<?> cls = findLoadedClass(name);
        if (cls == null) {
          try {
            cls = findClass(name);
          } catch (ClassNotFoundException cnfe) {
            // ignore, fallback to bootstrap classloader
          }
          if (cls == null) {
            cls = BOOTSTRAP_ONLY_CLASSLOADER.loadClass(name);
          }
        }
        if (resolve) {
          resolveClass(cls);
        }
        return cls;
      }
    }

    @Override
    public URL getResource(String name) {
      URL resource = findResource(name);
      if (resource == null) {
        BOOTSTRAP_ONLY_CLASSLOADER.getResource(name);
      }
      return resource;
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
      Enumeration<URL> selfResources = findResources(name);
      Enumeration<URL> bootstrapResources = BOOTSTRAP_ONLY_CLASSLOADER.getResources(name);
      if (!selfResources.hasMoreElements()) {
        return bootstrapResources;
      }
      if (!bootstrapResources.hasMoreElements()) {
        return selfResources;
      }
      ArrayList<URL> resources = Collections.list(selfResources);
      resources.addAll(Collections.list(bootstrapResources));
      return Collections.enumeration(resources);
    }

    // XXX: we know URLClassLoader#getResourceAsStream calls getResource, so we don't have to
    // override it here.
  }
}
