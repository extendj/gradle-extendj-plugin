# Gradle ExtendJ Plugin

This plugin configures `JavaCompile` tasks to use the [ExtendJ compiler].

[ExtendJ compiler]: https://extendj.org/

This project is a fork of a the [ErrorProne gradle plugin][1].


## Building and Using

To build and install the plugin to your local Maven repository:

    ./gradlew install


Use the plugin by adding this to the target project's `build.gradle`:

    buildscript {
      repositories {
        mavenLocal()
      }
      dependencies {
        classpath 'org.extendj:gradle-extendj-plugin:1.0'
      }
    }

    apply plugin: 'org.extendj'


It may also be useful to add these lines so that the very latest ExtendJ build is always used:

    configurations.extendj {
      resolutionStrategy.cacheChangingModulesFor 0, 'seconds'
    }


## References

[gradle issue 1652](https://github.com/gradle/gradle/issues/1652)
[error-prone issue 535](https://github.com/google/error-prone/issues/535)


[1]: https://github.com/tbroyer/gradle-errorprone-plugin
